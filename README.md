# LibWebC

LibWebC intends to provide a list of independant Web Component. They uses an
Abstract class allowing easier Web Components developments

## Milestones

| Milestone | Description |
| --------- | ----------- |
| WIP       | WIP         |

## Project Structure

The project tries to follow an organized structure to maintain code clarity and
modularity:

```bash
.
├── .vscode
│   └── *.json
├── deno.json
├── deno.lock
├── public
│   ├── assets
│   │   └── *.svg
│   ├── components
│   │   └── *.js
│   ├── index.html
│   ├── object
│   │   └── *.js
│   ├── style
│   │   └── *.css
│   ├── utils
│   │   └── *.js
│   └── views
│       ├── *.js
├── README.md
├── watcher.js
└── wiki
    └── template.html
```

### Key Components:

- public/assets: Images used for the example page.
- public/components: Web components, such as a Table component.
- public/style: CSS styles for the application.
- public/utils: Utility scripts.
- public/views: Main views of the application.
- deno.json and deno.lock: Deno configuration files.
- watcher.js: Utility for watching changes in a specified folder.
- wiki/template.html: Documentation template.
- .vscode: VsCode IDE configuration

## Getting Started

To run the application locally, follow these steps:

- Clone the repository:

```bash
git clone https://github.com/your-username/my-media-library.git
```

- Navigate to the project folder:

```bash
cd my-media-library
```

- Start the local development server:

```bash
deno task serve
```

Open your browser and go to http://localhost:8080 to access the application.

## Development

- Local Development Server: Utilizes Deno Archy for a lightweight local
  development server.
- Code Formatting and Linting: Integrated Deno linter and formatter for
  consistent code style.
- Testing: Includes unit tests to ensure the reliability of critical components.

## Contributing

Contributions are welcome! If you have ideas for new features, improvements, or
find any issues, feel free to create an issue or submit a pull request.

## License

Coming soon but main points are:

- Follow authors right by always citing the original repository and its authors,
  aka: LibShare repository and me Bastien Delseny
- You can modify and distribute it as you want as long as you respect the first
  point about authors right and always refer the original repository and authors
