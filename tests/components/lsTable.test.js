import { assertEquals } from "https://deno.land/std@0.177.0/testing/asserts.ts";
import {
  beforeEach,
  describe,
  it,
} from "https://deno.land/std@0.222.0/testing/bdd.ts";

describe("LsTable component test", () => {
  let table = {};
  beforeEach(async () => {
    class Element {
      innerHTML = "";
    }
    class HTMLElement extends Element {}
    globalThis.HTMLElement = HTMLElement;
    globalThis.document = {
      innerHTML: "",
      createElement: () => new HTMLElement(),
    };
    globalThis.customElements = { define: () => {} };

    const { LsTable } = await import("../../public/components/lsTable.js");
    table = new LsTable();
  });

  it("renders noData text when no rows passed", () => {
    table.noData = "No data";
    const template = table.template();
    assertEquals(template.innerHTML.includes("No data"), true);
  });

  it("renders rows when provided", () => {
    table.rows = [["a", 1], ["b", 2]];
    const template = table.template();
    assertEquals(template.innerHTML.includes("a"), true);
    assertEquals(template.innerHTML.includes("b"), true);
  });

  it("renders headers when provided", () => {
    table.headers = ["Letter", "Number"];
    table.rows = [["a", 1], ["b", 2]];
    const template = table.template();
    assertEquals(template.innerHTML.includes("Letter"), true);
    assertEquals(template.innerHTML.includes("Number"), true);
  });
});
