import { parse } from "https://deno.land/std/flags/mod.ts";

const args = parse(Deno.args);

console.log(`Watch "${args.folder}", to execute "${args.command}"`);

const watcher = Deno.watchFs(args.folder);

/** @type {string[]} */
const command = args.command.split(" ");

for await (const event of watcher) {
  const re = new RegExp(`${args.folder}/${args.exclude}`);
  let reload = false;
  event.paths.forEach((path) => {
    reload = !re.test(path) || reload;
  });
  if (reload) {
    const cmd = new Deno.Command(command[0], {
      args: command.slice(1, command.length),
    });
    const { code, stdout, stderr } = await cmd.output();
    console.log(new TextDecoder().decode(stdout));
    console.warn(new TextDecoder().decode(stderr));
    if (code != 0) {
      console.error(new TextDecoder().decode(stderr));
    }
  }
}
