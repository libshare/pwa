/**
 * Indexeddb Wrapper to easying the use of Inddexeddb
 *
 * @example
 * To initialize a Store, juste do
 * const db = new IndexedDBWrapper('MyStore');
 * Then use it
 * db.add(data);
 * db.get(id);
 * db.getAll();
 * db.update(id, data);
 * db.delete(id);
 * db.clear();
 */
export class IndexedDBWrapper {
  indexedDBChangeEvent = new CustomEvent("indexedDBChange");

  /**
   * @param {string} storeName the store name in the database
   */
  constructor(storeName) {
    this.dbName = "LibShare";
    this.storeName = storeName;
    this.db = null;
  }

  /**
   * Open the Indexeddb Database
   * @returns {Promise<IDBDatabase>} the Indexedb database
   */
  open() {
    if (this.db) {
      return this.db;
    }

    return new Promise((resolve, reject) => {
      const request = indexedDB.open(this.dbName, 1);

      request.onupgradeneeded = (event) => {
        const db = event.target.result;
        db.createObjectStore(this.storeName, {
          keyPath: "id",
          autoIncrement: true,
        });
      };

      request.onsuccess = (event) => {
        this.db = event.target.result;
        resolve(this.db);
      };

      request.onerror = (event) => {
        reject(event.target.error);
      };
    });
  }

  /**
   * Add the given object to the store
   * @param {Object} data data to add to the store
   * @returns {Promise<number>} the id of the added data
   * @throws {Error} if data is not an object
   */
  async add(data) {
    if (!data || typeof data !== "object") {
      throw new Error("Invalid data provided for add operation.");
    }

    const db = await this.open();
    const tx = db.transaction(this.storeName, "readwrite");
    const store = tx.objectStore(this.storeName);

    return new Promise((resolve, reject) => {
      const request = store.add(data);

      request.onsuccess = () => {
        document.dispatchEvent(this.indexedDBChangeEvent);
        resolve(request.result);
      };
      request.onerror = () => reject(request.error);
    });
  }

  /**
   * Get the element of the given key from the store
   * @param {number} key key of the element to retrieve from the store
   * @returns {Promise<Object>} the element of the given key
   * @throws {Error} if key is not a number
   */
  async get(key) {
    const db = await this.open();
    const tx = db.transaction(this.storeName);
    const store = tx.objectStore(this.storeName);

    return new Promise((resolve, reject) => {
      const request = store.get(key);

      request.onsuccess = () => resolve(request.result);
      request.onerror = () => reject(request.error);
    });
  }

  /**
   * Get all the elements from the store
   * @returns {Promise<Array<Object>>} all the elements from the store
   * @throws {Error} if the request cannot be fullfilled
   */
  async getAll() {
    const db = await this.open();
    const tx = db.transaction(this.storeName);
    const store = tx.objectStore(this.storeName);

    return new Promise((resolve, reject) => {
      const request = store.getAll();

      request.onsuccess = () => resolve(request.result);
      request.onerror = () => reject(request.error);
    });
  }

  /**
   * Delete the element of the given key from the store
   * @param {number} key key of the element to delete from the store
   * @returns {Promise<void>}
   * @throws {Error} if the request cannot be fullfilled
   */
  async delete(key) {
    const db = await this.open();
    const tx = db.transaction(this.storeName, "readwrite");
    const store = tx.objectStore(this.storeName);

    return new Promise((resolve, reject) => {
      const request = store.delete(key);

      request.onsuccess = () => {
        document.dispatchEvent(this.indexedDBChangeEvent);
        resolve(request.result);
      };
      request.onerror = () => reject(request.error);
    });
  }

  /**
   * Update the element given the key of the given data
   * @param {Object} data data to update the element with
   * @returns {Promise<number>} the id of the updated data
   * @throws {Error} data is not an object, or does not have an id
   */
  async put(data) {
    if (!data || typeof data !== "object") {
      throw new Error("Invalid data provided for put operation.");
    }

    const db = await this.open();
    const tx = db.transaction(this.storeName, "readwrite");
    const store = tx.objectStore(this.storeName);

    return new Promise((resolve, reject) => {
      const request = store.put(data);

      request.onsuccess = () => {
        document.dispatchEvent(this.indexedDBChangeEvent);
        resolve(request.result);
      };
      request.onerror = () => reject(request.error);
    });
  }
}
