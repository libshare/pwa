/**
 * Base configuration for components
 *
 * This follows the standard Web component implementation.
 *
 * @example
 * It is meant to be extended.
 *
 * class MyComponent extends Component {
 *   constructor() {
 *     super();
 *   }
 *
 *   template() {
 *     const template = document.createElement("template");
 *     template.innerHtml = `<div>Hello</div>`;
 *     return template;
 *   }
 * }
 * customElements.define("my-component", MyComponent);
 */
export class Component extends HTMLElement {
  constructor() {
    super();
  }

  async connectedCallback() {
    await this.render();
  }

  template() {
    return document.createElement("template");
  }

  async render() {
    this.innerHTML = "";
    this.appendChild((await this.template()).content.cloneNode(true));
    this.setupEventListeners();
  }

  setupEventListeners() {}

  async attributeChangedCallback() {
    await this.render();
  }
}
