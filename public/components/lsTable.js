import { Component } from "./component.js";

/**
 * Table component for easing the creation of tables
 *
 * @property {Array<Array<string>>} rows A list of cells of rows
 * @property {string[]|undefined} headers Optional, A list of headers
 * @property {string|undefined} noData Optional, A text to display when there is no data
 *
 * @example
 * Use it directly in your HTML with the `ls-table` tag
 *
 * <ls-table noData="no data provided" rows='[["a", 1], ["a", 1], ["a", 1], ["a", 1], ["a", 1]]' headers='["column 1", "column 2" ]'></ls-table>
 *
 * Arguments can also be passed using JavaScript
 *
 * this.noData = "no data provided";
 * this.rows = JSON.stringify([["a", 1], ["a", 1], ["a", 1], ["a", 1], ["a", 1]]);
 * this.headers = JSON.stringify(["column 1", "column 2" ]);
 * const html = `<ls-table noData='${this.noData}' rows='${this.rows}' headers='${this.headers}'></ls-table>`;
 */
export class LsTable extends Component {
  constructor() {
    super();
  }

  connectedCallback() {
    this.headers = JSON.parse(this.getAttribute("headers"));
    this.rows = JSON.parse(this.getAttribute("rows"));
    this.noData = this.getAttribute("noData");
    super.connectedCallback();
  }

  /**
   * Here is mapped the table using the given headers and inputs.
   *
   * There is a CSS media query for small screens:
   *
   * `@media only screen and (max-width: 760px)` which wraps headers
   * alongside each row value for easier readability
   */
  template() {
    this.headers = this.headers || [];
    this.noData = this.noData || "";
    const template = document.createElement("template");
    const style = /*css*/ `
      @media only screen and (max-width: 760px) {
        ${
      this.headers.map((header, i) => /*css*/ `
            td:nth-of-type(${i + 1}):before {
              content: "${header}";
              font-size: small;
              font-style: italic;
              text-transform: capitalize;
            }`).join("")
    }
      }
    `;
    template.innerHTML = /*html*/ `
      <table class="custom-table">
        <thead>
          <tr>
            ${this.headers.map((header) => `<th>${header}</th>`).join("")}
          </tr>
        </thead>
        <tbody>
          ${
      this.rows
        ? this.rows.map((row) => `<tr>${this.setRow(row).join("")}</tr>`).join(
          "",
        )
        : this.noData
    }
        </tbody>
      </table>
      <style>${style}</style>
    `;
    return template;
  }

  /**
   * Set the row of the table
   *
   * @param {Array|Object} row a row of data
   * @returns {Array<string>} an array of table cells
   */
  setRow(row) {
    if (Array.isArray(row)) {
      return row.map((data) => `<td>${data}</td>`);
    } else {
      const tds = [];
      for (const cell in row) {
        tds.push(`<td>${row[cell]}</td>`);
      }
      return tds;
    }
  }
}

customElements.define("ls-table", LsTable);
